/**
 * Created by ikey on 2019-08-23.
 * UpdateTime on
 */

import Vuex from 'vuex'
import user from './user'
import events from "./events";
import home from "./home";

const store = () => new Vuex.Store({
    modules: {
        user,
        events,
        home,
    }
});

export default store
