export default {
  state: {
    info: {},
  },
  mutations: {
    muInfo(state, data) {
      state.info = data;
    }
  },
  actions: {
    acInfo({commit, state}, data) {
      return new Promise(((resolve, reject) => {
        setTimeout(() => {
          if (data !== null) {
            resolve(true);
            commit('muInfo', data);
          } else {
            reject(false);
          }
        }, 1000)
      }))
    },
    nuxtServerInit({commit, state}, {req}) {
      // commit('user', req.session.user);
    }
  }
}
