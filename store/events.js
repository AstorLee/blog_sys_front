export default {
  state:{
    data:{
      follow:{
        dataArr:[],
        msgNum:0,
      },
      comment:{
        dataArr:[],
        msgNum:0,
      },
      like:{
        dataArr:[],
        msgNum:0,
      },
      collect:{
        dataArr:[],
        msgNum:0,
      },
      msgNum:0
    }
  },
  mutations:{
    muData(state,data) {
      state.data = data;
    }
  },
  actions:{
    acData({commit,state},data) {
      commit("muData",data);
    },
    acClearData({commit,state}) {
      let params = {
        follow:{
          dataArr:[],
          msgNum:0,
        },
        comment:{
          dataArr:[],
          msgNum:0,
        },
        like:{
          dataArr:[],
          msgNum:0,
        },
        collect:{
          dataArr:[],
          msgNum:0,
        },
        msgNum:0
      };
      return new Promise( (resolve, reject) => {
        commit("muData",params);
        setTimeout(() => {
          resolve(true);
        },1000)
      } )
    }
  }
}
