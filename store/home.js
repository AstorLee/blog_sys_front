export default {
    state: {
        rangeId: 1,
        themeId: '',
    },
    mutations: {
        muRangeId(state, data) {
            state.rangeId = data;
        },
        muThemeId(state, data) {
            state.themeId = data;
        }
    },
    actions: {
        acRangeId({commit, state}, data) {
            commit('muRangeId', data);
        },
        acThemeId({commit, state}, data) {
            commit('muThemeId', data);
        },
    }
}
