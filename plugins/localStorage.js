/**
 * Created by ikey on 2019-07-01.
 * UpdateTime on
 */
import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {
  createPersistedState({
    storage: window.localStorage
  })(store)
}
