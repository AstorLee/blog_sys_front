import {setCookie, getCookie, delCookie} from "../utils";

export default function (app) {
  let vm = app.store._vm;
  let router = app.app.router;
  let axios = app.$axios;

  axios.onRequest( config => {

    if ( process.browser ) {
      let jwt = config.headers.common.jwt;
      if ( jwt ) {
        setCookie("jwt",jwt,1);
        axios.setHeader("jwt",jwt);
      }else {
        let cookieJwt = getCookie("jwt");
        if ( cookieJwt ) {
          axios.setHeader("jwt",cookieJwt);
        }
      }
    }
  } );

  axios.onResponse( response => {
    if ( response.data.code === 1002 ) {
      vm.$notify.error({
        title:"错误",
        message:response.data.message,
      });
      router.push("/login");
      localStorage.clear();
      delCookie("jwt");
    }
  } )

}
